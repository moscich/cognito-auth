import json

import boto3

client = boto3.client('cognito-idp')

user = 'demo18@example.com'
client.sign_up(
    ClientId='5gk540u5sblq270ar3v1ff286f',
    Username=user,
    Password='TestUse1!@{23rXD',
    ValidationData=[
        {
            'Name': 'Applecostam',
            'Value': 'secretstring'
        }
    ]
)

response = client.initiate_auth(
    # AuthFlow='USER_PASSWORD_AUTH',
    AuthFlow='CUSTOM_AUTH',
    AuthParameters={
        'USERNAME': user,
        'token': 'eucostam'
    },
    ClientMetadata={
        'Something': 'trupa'
    },
    UserContextData = {
        'EncodedData': 'value'
    },
    ClientId='5gk540u5sblq270ar3v1ff286f'

)

session = response['Session']
challenge_name = response['ChallengeName']

response = client.respond_to_auth_challenge(
    ClientId='5gk540u5sblq270ar3v1ff286f',
    ChallengeName='CUSTOM_CHALLENGE',
    Session=session,
    ChallengeResponses={
        "ChallengeName": challenge_name,
        # "ChallengeResponses": json.dumps({"USERNAME": user, "ANSWER": "Tu odpowiedz"}),
        "USERNAME":user,
        "ANSWER": "SUPER_ANSWER"
    },
    # AnalyticsMetadata={
    #     'AnalyticsEndpointId': 'string'
    # },
    # UserContextData={
    #     'IpAddress': 'string',
    #     'EncodedData': 'string'
    # },
    # ClientMetadata={
    #     'string': 'string'
    # }
)

print(response)
