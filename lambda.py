import json


def lambda_handler(event, context):
    print(json.dumps(event))
    if event['triggerSource'] == 'CreateAuthChallenge_Authentication':
        event['response']['publicChallengeParameters'] = {'test': 'test'}
        event['response']['privateChallengeParameters'] = {'test2': 'test2'}
        event['response']['challengeMetadata'] = 'test3'
        print('simple return')
        return event

    if event['triggerSource'] == 'PreSignUp_SignUp':
        event['response']['autoConfirmUser'] = True

    if event['triggerSource'] == 'DefineAuthChallenge_Authentication':

        if len(event['request']['session']) > 0:
            event['response']['issueTokens'] = True
            event['response']['failAuthentication'] = False
            event['response'].pop('challengeName')
        else:
            event['response']['challengeName'] = 'CUSTOM_CHALLENGE'
            event['response']['issueTokens'] = False
            event['response']['failAuthentication'] = False

    if event['triggerSource'] == 'VerifyAuthChallengeResponse_Authentication':
        event['response']['answerCorrect'] = True

    print(json.dumps(event))
    return event
